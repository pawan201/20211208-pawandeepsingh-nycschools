//
//  ViewController.swift
//  20211204-PawandeepSingh-NYCSchools
//
//  Created by Pawandeep Singh on 12/4/21.
//

import UIKit

class NYCSchoolsList: UITableViewController {
    @IBOutlet var hs_list: UITableView!
    
    // Retrives data for two column 'dnb' and 'school_name', since the others aren't needed
    // if need to, could add more in the line below
    var fields: String = "$select=dbn,school_name"
    // Pagination: retrives 20 items per fetch upon scroll
    var limit = 20
    // Pagination: data starts at 0th item
    var offset = 0
    // Data is being sorted by "school_name" ASC,
    // in the future you could add menu bar or item bar to allow user to sort data by Borough, or graduation_rate and etc...
    var order_by = "school_name"
    
    var isDataLoading:Bool=false
    var nycSchoolsList:Array = [Dictionary<String, String>]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getNycSchools()
        hs_list.delegate = self
        hs_list.dataSource = self
    }
    
    func getNycSchools(){
        var request = URLRequest(url: URL(string: "https://data.cityofnewyork.us/resource/s3k6-pzi2.json?\(fields)&$limit=\(limit)&$offset=\(offset)&$order=\(order_by)")!)
        request.httpMethod = "GET"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let session = URLSession.shared
        let task = session.dataTask(with: request, completionHandler: { data, response, error -> Void in
            do {
                let json = try JSONSerialization.jsonObject(with: data!) as! [Dictionary<String, String>]
                self.nycSchoolsList.append(contentsOf: json)
                DispatchQueue.main.async {
                    self.hs_list.reloadData()
                }
            } catch {
                print("error")
            }
        })
        
        task.resume()
        
        
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "NYC Schools"
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return nycSchoolsList.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "main", for: indexPath)
        // check for network, if avialble, processeds to show data else displays "Network Issue"
        if Reachability.isConnectedToNetwork(){
            cell.textLabel?.text = self.nycSchoolsList[indexPath.row]["school_name"]
        }
        else {
            cell.textLabel?.text = "Network Issue"
        }
        return cell
    }
    
    override func  tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if nycSchoolsList.count != 0{
            performSegue(withIdentifier: "sats", sender: nil)
        }
    }
    
    // Data being passed to SATScoresViewController using segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? SatScoresVC {
            destination.dbn = nycSchoolsList[hs_list.indexPathForSelectedRow!.row]["dbn"]!
            destination.school_name = nycSchoolsList[hs_list.indexPathForSelectedRow!.row]["school_name"]!
            
        }
    }
    
    //Pagination is implemented to prevent all records being displayed at once,
    //which will take longer to load and a user may not even get to see it all
    override func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        isDataLoading = false
    }
    
    override func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
    }
    
    override func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if ((tableView.contentOffset.y + tableView.frame.size.height) >= tableView.contentSize.height)
        {
            if !isDataLoading{
                isDataLoading = true
                self.offset = nycSchoolsList.count
                getNycSchools()
            }
        }
    }
}

