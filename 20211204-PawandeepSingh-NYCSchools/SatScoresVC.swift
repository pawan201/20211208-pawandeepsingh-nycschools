//
//  SatScoresVC.swift
//  20211204-PawandeepSingh-NYCSchools
//
//  Created by Pawandeep Singh on 12/7/21.
//

import UIKit

class SatScoresVC: UIViewController{
    var dbn: String = ""
    var school_name: String = ""
    
    @IBOutlet weak var mathScore: UILabel!
    @IBOutlet weak var readingScore: UILabel!
    @IBOutlet weak var writingScore: UILabel!
    @IBOutlet weak var schoolName: UILabel!
    
    var sat_scores = [SatScoresModel]()
    
    func getHsSatScores(dbn_code: String){
        var request = URLRequest(url: URL(string: "https://data.cityofnewyork.us/resource/f9bf-2cp4.json?dbn=\(dbn_code)")!)
        request.httpMethod = "GET"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        let session = URLSession.shared
        let task = session.dataTask(with: request, completionHandler: { data, response, error -> Void in
            do {
                let json = try JSONSerialization.jsonObject(with: data!) as! [Dictionary<String, String>]
                self.sat_scores.append(SatScoresModel(math: json[0]["sat_math_avg_score"] ?? " ", reading: json[0]["sat_critical_reading_avg_score"] ?? " ", writing: json[0]["sat_writing_avg_score"] ?? " "))
                DispatchQueue.main.async {
                    self.mathScore.text =  self.sat_scores[0].math_scr
                    self.readingScore.text =  self.sat_scores[0].reading_scr
                    self.writingScore.text =  self.sat_scores[0].writing_scr
                    self.schoolName.text = self.school_name
                }
            } catch {
                print("error")
            }
        })
        task.resume()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getHsSatScores(dbn_code: dbn)
    }
}
