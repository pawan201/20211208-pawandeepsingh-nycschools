//
//  SatScores_Model.swift
//  20211204-PawandeepSingh-NYCSchools
//
//  Created by Pawandeep Singh on 12/7/21.
//

import Foundation

class SatScoresModel{

    var math_scr: String =  ""
    var reading_scr: String =  ""
    var writing_scr: String =  ""
    
    init(math: String,reading: String,writing: String) {
        self.math_scr = math
        self.reading_scr = reading
        self.writing_scr = writing
    }
    
}
