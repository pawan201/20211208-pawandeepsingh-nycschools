//
//  _0211204_PawandeepSingh_NYCSchoolsTests.swift
//  20211204-PawandeepSingh-NYCSchoolsTests
//
//  Created by Pawandeep Singh on 12/4/21.
//

import XCTest
@testable import _0211204_PawandeepSingh_NYCSchools

class _0211204_PawandeepSingh_NYCSchoolsTests: XCTestCase {
    
    func testSatScores() throws {
        var sat_scores = [SatScoresModel]()
        sat_scores.append(SatScoresModel(math: "222", reading: "333", writing: "444"))
        XCTAssertTrue(sat_scores[0].math_scr == "222")
        XCTAssertTrue(sat_scores[0].reading_scr == "333")
        XCTAssertTrue(sat_scores[0].writing_scr == "444")
       }
    
}
